# Project Title

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [License](./LICENSE)

## About

Maven quickstart archetype using Junit5 and Java SE 14

## Installation

1. Clone the repository locally
2. Run the below maven command to install the archetype:
```shell
$ mvn install
```

## Usage
Use the below maven command, replacing value as needed:
```shell
$ mvn archetype:generate -DarchetypeGroupId=com.ninomaruszewski.archetypes
                         -DarchetypeArtifactId=java14-junit5-quickstart
                         -DarchetypeVersion=1.0.0
```
The rest of the generation will be done interactively.